#include <conio.h>
#include <iostream>

using std::cout;
using std::endl;

int main()
{
    cout << "Hello Vova and Roman!" << endl;
    cout << "Vladyslav Sagadin"     << endl;
    cout << "tel: 570 884 859"      << endl;
    cout << "os. Oswiecenia 65/4"   << endl;
    cout << "61-209 Poznan"         << endl;
    cout << "Pozdrawiam!"           << endl;

    _getch();

    return 0;
}