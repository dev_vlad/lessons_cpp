#include <iostream>

using namespace std;

int main()
{
    double celsiusDegrees;
    double fahrenheitDegrees;

    cout << "Enter temperature at Celsius degrees: " << endl;
    cin >> celsiusDegrees;

    fahrenheitDegrees = (celsiusDegrees * 1.8) + 32;
    cout << "the temperature in Fahrenheit is: " << fahrenheitDegrees << endl;

    cin.get();

    return 0;
}