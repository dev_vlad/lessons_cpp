#include <iostream>

using namespace std;

int main()
{
    double pln, dollar, euro;
    const double plnPerDollar = 3.67;
    const double plnPerEuro = 4.31;

    cout << "Enter number of Pln: " << endl;
    cin >> pln;

    dollar = pln / plnPerDollar;
    euro   = pln / plnPerEuro;

    cout << dollar << " $ " << endl << euro << " euro " << endl;

    cin.get();

    return 0;
}
